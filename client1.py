#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys,os
import socket
import time
import threading

msg=0
try:
    msg=sys.argv[1]
except:
    pass

HOST="180.76.147.23"
# HOST="127.0.0.1"
#HOST='133.130.89.147'
#HOST='192.168.8.111'
#HOST='192.168.8.106'
PORT=50091
 
with socket.socket(type=socket.SOCK_STREAM) as s:
    s.connect((HOST,PORT))
    if(msg==0):
        try:
            s.sendall(b'open')
        except:
            exit()  # restart ?
        data = s.recv(128) 
        print(repr(data))
        if(data[0:6] == b'opened'):
            print("client1 recieve that client0 has opened")
            print("now you can connect to nat ssh port:"+str(data[6:10]))
        else:
            print("error")
    else:
        umsg=bytes(msg,'utf-8')
        try:
            s.sendall(umsg)
        except:
            exit()  # restart ?
        data = s.recv(128) 
        print(repr(data))
        if(data[0:6] == b'msg0ed'):
            print("client1 recieve that client0 has msg0ed")
            print("now you can connect to nat ssh port:"+str(data[6:10]))
        else:
            print("error")




