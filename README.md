#Concept 
client0  you machine that controled by remoting
server0  vps
client1  any machine you are working on

#Use Method
1. ssh config
server0:
    sudo vim /etc/ssh/sshd_config --> GatewayPorts yes
client0:
    generate a certicate, such as rsa
    ssh-copy-id -i ~/.ssh/id_rsa.pub  <vps@ip>


client0.py will deploy in the controled machine
server0.py will deploy in the vps

then you can use client1.py in any machine to control client0 machine

# Command
## use default port 9030
python3 client1.py 
ssh -p 9030 <remote@ip>
## use other port
python3 client1.py msg09032
ssh -p 9032 <remote@ip>  (remote is the user of client0, the ip is in vps, and password is about of client0)


## auto start in booting
__First__
cd /lib/systemd/system/
vim rc-local.service (add in the end)

[Install]
WantedBy=multi-user.target  ## __will start after boot__
__Second__
sudo ln -fs /lib/systemd/system/rc-local.service /etc/systemd/system/rc-local.service
__third__
put your code in rc.local (you may create it )
chmod +x /etc/rc.local
**example:**  (note: #!/bin/bash and exit 0 must to use)
#!/bin/bash

echo  $USER > /home/ka/cenv/start.txt  ## the $USER is null ?
echo  'heel' >> /home/ka/cenv/start.txt

su ka -c echo 'uuu' > /home/ka/cenv/ooo  ## will be root ,not ka

if [ $USER == 'root' ]; then
        echo root > /home/ka/cenv/user.txt
elif [ $USER == 'ka' ]; then
        echo ka > /home/ka/cenv/user.txt
fi

exit 0

__finnal__
sudo systemctl restart rc-local.service  ( note: if service is active ,must use restart, if use start, it will have no effect)
(__note__, 1 you may be to use ssh -i option to pin you identity file as when you use
service the role will no be your wanted role )

## History
2018.8.17
add autostart in booting
first to use /etc/rc.local  --> not executed
second to use update-rc.d  command , then use service manual start, but role is root ,i want role is not root
ret: https://www.linuxidc.com/Linux/2017-09/147166.htm
third to use /etc/profile , role is not root, but when logining in by ssh, it will execute it that i don't expect to 
