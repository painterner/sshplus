import sys,os
import socket
import threading
import time

# note: must use python3
class value():
    def __init__(self):
        self.v = b''
    def setv(self,value):
        self.v = value
    def getv(self):
        return self.v

value=value()

class sHandle(threading.Thread):
    def __init__(self,HOST,PORT,dataLen):
        threading.Thread.__init__(self)
        self.maxHtbtLen = 10 
        self.HOST = HOST
        self.PORT = PORT
        self.dataLen = dataLen
        self.dataL = ['',False]
        
    def run(self):
        with socket.socket(type=socket.SOCK_STREAM) as s:
            s.bind((self.HOST,self.PORT))
            s.listen(2)
            while True:
                    conn,addr = s.accept()
                    print(addr)
                    with conn:
                        while True:
                            data = conn.recv(self.dataLen) 
                            if not data: break
                            if(value.getv() == b'open'):
                                conn.sendall(b'open')
                                if(conn.recv(self.dataLen) == b'opened'):
                                    print('client0 opened, to notify server thread2')
                                    value.setv(b'opened')
                            else:
                                print(data)
                                if(data == b'A'):
                                    time.sleep(2)
                                    conn.sendall(b'B')
    def __call__(self,data):
        self.dataL[1] = data
        self.dataL[0] = True


class s2Handle(threading.Thread):
    def __init__(self,HOST,PORT,dataLen):
        threading.Thread.__init__(self)
        self.maxHtbtLen = 10 
        self.HOST = HOST
        self.PORT = PORT
        self.dataLen = dataLen
        self.dataL = ['',False]
        
    def run(self):
        with socket.socket(type=socket.SOCK_STREAM) as s:
            s.bind((self.HOST,self.PORT))
            s.listen(2)
            while True:
                    conn,addr = s.accept()
                    print(addr)
                    with conn:
                        while True:
                            data = b''
                            try:
                                data = conn.recv(self.dataLen)
                            except:
                                conn.close()
                                break

                            if not data: break
                            # if(self.dataL[0] == True):
                                # self.dataL[0] = False
                                # conn.sendall(self.dataL[1])
                            # else:
                            if(data == b'open'):
                                value.setv(b'open')
                            else:
                                conn.close()
                                break
                            while value.getv() != b'opened':
                                time.sleep(0.5)
                            print('server thread2 recv, to notify client1')
                            conn.sendall(b'opened')
                            value.setv(b'')
    def __call__(self,data):
        self.dataL[1] = data
        self.dataL[0] = True

if __name__ == "__main__":
    th1 = sHandle('0.0.0.0',50080,10)
    th1.start()
    th2 = s2Handle('0.0.0.0',50091,10)
    th2.start()

